package net.iescierva.ramonmr95.p0301sumaexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private Button btnCalcularDistancia;
    private EditText g1, g2, m1, m2, s1, s2, g12, m12, s12, g22, m22, s22;
    private TextView rtv;
    private Locale l;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponentes();
    }

    public void initComponentes() {
        btnCalcularDistancia = findViewById(R.id.btnCalcularDistancia);
        g1 = findViewById(R.id.g1);
        g2 = findViewById(R.id.g2);
        m1 = findViewById(R.id.m1);
        m2 = findViewById(R.id.m2);
        s1 = findViewById(R.id.s1);
        s2 = findViewById(R.id.s2);

        g12 = findViewById(R.id.g12);
        g22 = findViewById(R.id.g22);
        m12 = findViewById(R.id.m12);
        m22 = findViewById(R.id.m22);
        s12 = findViewById(R.id.s12);
        s22 = findViewById(R.id.s22);

        rtv = findViewById(R.id.resulttv);

        l = Locale.getDefault();
    }

    public void calcularDistancia(View view) {
        try {
            GeoPunto gp1 = new GeoPunto(Double.parseDouble(g1.getText().toString()),
                    Double.parseDouble(m1.getText().toString()),
                    Double.parseDouble(s1.getText().toString()),
                    Double.parseDouble(g2.getText().toString()),
                    Double.parseDouble(m2.getText().toString()),
                    Double.parseDouble(s2.getText().toString()));

            GeoPunto gp2 = new GeoPunto(Double.parseDouble(g12.getText().toString()),
                    Double.parseDouble(m12.getText().toString()),
                    Double.parseDouble(s12.getText().toString()),
                    Double.parseDouble(g22.getText().toString()),
                    Double.parseDouble(m22.getText().toString()),
                    Double.parseDouble(s22.getText().toString()));

            double distancia = gp1.distancia(gp2) / 1000;
            rtv.setText(String.format(l, "%s: %.3f Km", getString(R.string.distance), distancia));
        }
        catch (Exception e) {
            rtv.setText(R.string.error_validacion);
        }

    }

    public void borrar(View view) {
        g1.setText("");
        g2.setText("");
        m1.setText("");
        m2.setText("");
        s1.setText("");
        s2.setText("");

        g12.setText("");
        g22.setText("");
        m12.setText("");
        m22.setText("");
        s12.setText("");
        s22.setText("");

        rtv.setText("");;
    }
}
