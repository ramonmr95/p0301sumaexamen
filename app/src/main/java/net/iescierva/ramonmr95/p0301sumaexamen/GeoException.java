package net.iescierva.ramonmr95.p0301sumaexamen;

public class GeoException extends Exception {

    public GeoException(String s) {
        super(s);
    }

}
